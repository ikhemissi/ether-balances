# Thought process, assumptions, and technical decisions

## API
I went for REST intially because I was more familiar with it, but the more I worked with transactions, the more I think choosing GraphQL (or similar) would be more appropriate as transactions have a large description (many fields) which may or may not be needed by the website or the App that will make use of the API.
The current implementation is still based on REST.

## Database
I chose MongoDB as the queries made to the API are always related to one single user (for balance and transactions) which makes the `document` model perfect for it: 1 document, 1 user account.
Midway, I ended up splitting the document into 2: one to hold the basic user details (metadata + balance + transaction references), and another one for transactions. 
This decision was mainly motivated by the difficulty (or rather the complexity and low performance) of filtering transactions inside one collection, with the aggregation framework (or even worse doing the filtering on the Javascript side).
With the new data split, we can use the basic MongoDB selection operators to allow filtering transactions by date and by block number.

## Etherscan
Instead of using a module from `npmjs` to communicate with Etherscan, I created the module myself, for 2 reasons:
- `https://etherscan.io/apis` was mentioned in the assignment (instead of just Etherscan.io) so I thought calling the endpoints directly and parsing its results was part of the assignment.
- the existing modules come with predefined settings (except for the API key) and do not allow specifying pages and etherscan API URL for example (if we want to do tests with a testnet).

## Balances
In the assignment, it was mentioned that we need to store and find the user balances.
I can see different ways to interpret balances (in plural):
- Wei/Eth
- Wei/Eth + value estimates in Fiat (e.g. USD, EUR, GBP)
- Wei/Eth + ERC20 tokens
- Wei/Eth balance for every single transaction (after the execution of the transaction), eventually with ERC20 tokens too

The first and second interpretations can fit in the 3/5 hours of work, but the other 2 may need more time to do. 
In addition, the 4th solution may require a more complex approach because:
- There is a rate limit on the API of Etherscan (5 req/s), what to do when we have few accounts with thousands of transactions.
- Calling the API of Etherscan to get a balance for every single operation will make the indexing operation super slow.
These issues will not be present if we were using a local node and calling directly (rpc ?) to get the balance and to process new transactions immediately when they are mined.

The solution I was thinking of doing was to fetch all the internal and normal transactions from Etherscan, then iterate through these transactions and calculate the new balance after evey operation.
In the end, I went for the second solution (Wei/Eth + Fiat), and I also started working on the algorithm of the 4th solution.