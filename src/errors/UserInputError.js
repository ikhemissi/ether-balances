class UserInputError extends Error {
  constructor(message, originalError) {
    super(message);
    this.originalError = originalError;
    this.statusCode = 400;
  }
}

module.exports = UserInputError;
