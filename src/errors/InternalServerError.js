class InternalServerError extends Error {
  constructor(message, originalError) {
    super(message);
    this.originalError = originalError;
    this.statusCode = 500;
  }
}

module.exports = InternalServerError;
