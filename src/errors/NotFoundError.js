class NotFoundError extends Error {
  constructor(message, originalError) {
    super(message);
    this.originalError = originalError;
    this.statusCode = 404;
  }
}

module.exports = NotFoundError;
