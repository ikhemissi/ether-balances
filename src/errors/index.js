const InternalServerError = require('./InternalServerError');
const MongoError = require('./MongoError');
const UserInputError = require('./UserInputError');
const NotFoundError = require('./NotFoundError');

module.exports = {
  InternalServerError,
  MongoError,
  UserInputError,
  NotFoundError,
};
