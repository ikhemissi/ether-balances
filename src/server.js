const config = require('config');
const api = require('./api');
const database = require('./database');
const logger = require('./utils/logger');

database.connect();

process.on('exit', () => {
  logger.error('Received EXIT');
});

process.on('SIGINT', () => {
  logger.error('Received SIGINT');
  process.exit(2);
});

api.listen(config.server.port, (error) => {
  if (error) {
    logger.error(error);
    process.exit(1);
  }

  logger.info('Server started', {
    environment: config.environment,
    port: config.server.port,
  });
});
