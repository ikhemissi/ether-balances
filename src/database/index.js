const config = require('config');
const mongoose = require('mongoose');
const logger = require('../utils/logger');
const MongoError = require('../errors/MongoError');
const {
  getOrCreateAccount,
  updateAccount,
  getAccountFullDetails,
  getAccountOverview,
  getAddresses,
} = require('./account');

const {
  addTransactions,
  getTransactions,
} = require('./transaction');

mongoose.Promise = Promise;

mongoose.connection.on('error', (error) => {
  logger.error(new MongoError('Connection error', error));
});

mongoose.connection.once('open', () => {
  logger.info('Connected to MongoDB');
});

function connect(connectionString = config.mongodb.connectionString) {
  return mongoose.connect(connectionString, { useCreateIndex: true, useNewUrlParser: true, useFindAndModify: false });
}

function disconnect() {
  return mongoose.disconnect();
}

async function addNewTransactions({ address, balance, lastBlock, newTransactions }) {
  const transactionIds = await addTransactions({ address, newTransactions });

  return updateAccount({ address, balance, lastBlock, newTransactions: transactionIds });
}

module.exports = {
  connect,
  disconnect,
  getOrCreateAccount,
  getAccountOverview,
  getAccountFullDetails,
  getAddresses,
  addNewTransactions,
  getTransactions,
};
