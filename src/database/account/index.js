const Account = require('./model');

function createAccount({ address, balance, lastBlock }) {
  return Account.create({
    address,
    balance,
    lastBlock,
    transactions: [],
  });
}

async function getOrCreateAccount({ address, balance, lastBlock }) {
  const account = await Account.findOne({ address });

  if (account) {
    return account;
  }

  return createAccount({ address, balance, lastBlock });
}

function getAccountOverview({ address, fullTransactions = false }) {
  const transactionPopulationFilter = fullTransactions ? '-_id -__v' : 'date hash from to value -_id';

  return Account.findOne({ address })
    .populate({
      path: 'transactions',
      select: transactionPopulationFilter,
      options: { limit: 100 },
    });
}

function updateAccount({ address, balance, lastBlock, newTransactions = [] }) {
  const updateData = {
    updatedAt: new Date(),
    balance,
    $push: {
      transactions: {
        $each: newTransactions.sort((tx1, tx2) => tx2.date - tx1.date), // the newest at the top
        $position: 0,
      },
    },
  };

  if (lastBlock) {
    updateData.lastBlock = lastBlock;
  }

  return Account.findOneAndUpdate({ address }, updateData);
}

function getAddresses() {
  return Account.find({}, 'address updatedAt balance -_id');
}

module.exports = {
  getOrCreateAccount,
  updateAccount,
  getAccountOverview,
  getAddresses,
};
