const mongoose = require('mongoose');

const { Schema } = mongoose;

const schema = new Schema({
  address: { type: String, required: true, unique: true },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, required: false },
  balance: { type: String, default: '0' },
  lastBlock: { type: String, default: 0 }, // String instead of Number because this value will eventually exceed MAX_SAFE_INTEGER (but not any time soon)
  transactions: [{ type: Schema.Types.ObjectId, ref: 'Transaction' }],
});

const Account = mongoose.model('Account', schema);

module.exports = Account;
