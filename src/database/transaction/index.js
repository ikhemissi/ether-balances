const Transaction = require('./model');

function addTransaction(transaction) {
  return Transaction.create(transaction);
}

async function addTransactions({ address, newTransactions }) {
  const transactionsWithAddress = newTransactions.map(transaction => Object.assign({ address }, transaction));
  const transactions = await Promise.all(transactionsWithAddress.map(addTransaction));
  const transactionIds = transactions.map(transaction => transaction.id);

  return transactionIds;
}

function getTransactions({ address, fromDate, toDate, fromBlock, toBlock, fullTransactions = false, sort = 'newest' }) {
  const fieldsToExtract = fullTransactions ? '-_id -__v' : 'date hash from to value blockNumber -_id';
  const filters = { address };

  if (fromDate) {
    filters.date = filters.date || {};
    filters.date.$gte = fromDate;
  }

  if (toDate) {
    filters.date = filters.date || {};
    filters.date.$lte = toDate;
  }

  if (fromBlock) {
    filters.blockNumber = filters.blockNumber || {};
    filters.blockNumber.$gte = fromBlock;
  }

  if (toBlock) {
    filters.blockNumber = filters.blockNumber || {};
    filters.blockNumber.$lte = toBlock;
  }

  const dateSortingSetting = sort === 'newest' ? -1 : 1;

  return Transaction.find(filters, fieldsToExtract)
    .sort({ date: dateSortingSetting });
}

module.exports = {
  addTransaction,
  getTransactions,
  addTransactions,
};
