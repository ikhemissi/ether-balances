const mongoose = require('mongoose');

const { Schema } = mongoose;

const schema = new Schema({
  address: { type: String },
  date: { type: Date },
  blockNumber: { type: Number },
  timeStamp: { type: Number },
  hash: { type: String },
  nonce: { type: Number },
  blockHash: { type: String },
  transactionIndex: { type: Number },
  from: { type: String },
  to: { type: String },
  value: { type: Number },
  gas: { type: Number },
  gasPrice: { type: Number },
  isError: { type: String },
  contractAddress: { type: String },
});

const Transaction = mongoose.model('Transaction', schema);

module.exports = Transaction;
