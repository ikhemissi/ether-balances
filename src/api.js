const express = require('express');
const cors = require('cors');
const compression = require('compression');
const responseTime = require('response-time');
const routers = require('./routers');
const logger = require('./utils/logger');

const api = express();

api
  .disable('x-powered-by')
  .disable('etag')
  .use(cors())
  .use(compression())
  .use(responseTime())
  .use(logger.accessMiddleware)
  .use(routers)
  .use(logger.errorMiddleware);

module.exports = api;
