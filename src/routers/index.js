const { Router } = require('express');
const health = require('./health');
const accounts = require('./accounts');

const router = new Router();

router.use('/health', health);
router.use('/accounts', accounts);

module.exports = router;
