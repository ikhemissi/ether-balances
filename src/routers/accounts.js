const { Router } = require('express');
const { isValidAddress } = require('ethereumjs-util');
const logger = require('../utils/logger');
const database = require('../database');
const lastBlockPlusOne = require('../utils/lastBlockPlusOne');
const etherscan = require('../etherscan');
const getBalanceValue = require('../utils/getBalanceValue');
const transactionWithDate = require('../utils/transactionWithDate');
const { UserInputError, NotFoundError } = require('../errors');

const router = new Router();

router.get('/:address/transactions', async (req, res) => {
  const { address } = req.params;
  const { details, fromDate, toDate, fromBlock, toBlock, sort } = req.query;

  try {
    if (!isValidAddress(address)) {
      throw new UserInputError('Invalid Ethereum address');
    }

    const fullTransactions = details === 'full';
    const filteredTransactions = await database.getTransactions({
      address,
      fromDate,
      toDate,
      fromBlock,
      toBlock,
      fullTransactions,
      sort,
    });

    res.send(filteredTransactions);
  } catch (error) {
    res.status(error.statusCode || 500).send({ message: error.message });
  }
});

router.get('/:address', async (req, res) => {
  const { address } = req.params;
  const { transactions, exchange } = req.query;

  try {
    if (!isValidAddress(address)) {
      throw new UserInputError('Invalid Ethereum address');
    }

    const fullTransactions = transactions === 'full';
    const convertBalanceToOtherCurrencies = exchange === 'true';
    const account = await database.getAccountOverview({ address, fullTransactions });

    if (!account) {
      throw new NotFoundError('This address has yet to be indexed');
    }

    res.send({
      address: account.address,
      createdAt: account.createdAt,
      updatedAt: account.updatedAt,
      balance: await getBalanceValue({ wei: account.balance, toOtherCurrencies: convertBalanceToOtherCurrencies }),
      lastBlock: account.lastBlock,
      transactions: account.transactions,
      timestamp: Date.now(),
    });
  } catch (error) {
    res.status(error.statusCode || 500).send({ message: error.message });
  }
});

router.post('/', async (req, res) => {
  const { address } = req.query;

  try {
    if (!isValidAddress(address)) {
      throw new UserInputError('Invalid Ethereum address');
    }

    logger.info('Creating a new snapshot', { address });

    const account = await database.getOrCreateAccount({ address });
    const fromBlock = lastBlockPlusOne(account.lastBlock);
    const [transactions, balance] = await Promise.all([
      etherscan.getTransactions({ address, fromBlock }),
      etherscan.getAccountBalance({ address }),
    ]);

    const newTransactions = transactions.map(transaction => transactionWithDate({ transaction }));
    const lastTransaction = newTransactions[newTransactions.length - 1];
    await database.addNewTransactions({
      address,
      balance,
      lastBlock: lastTransaction ? lastTransaction.blockNumber : undefined,
      newTransactions,
    });

    res.send({
      message: `The account was updated with ${newTransactions.length} transactions`,
      address,
      balance,
      lastBlock: lastTransaction ? lastTransaction.blockNumber : undefined,
      newTransactions,
    });
  } catch (error) {
    res.status(error.statusCode || 500).send({ message: error.message });
  }
});

router.get('/', async (_, res) => {
  try {
    const accounts = await database.getAddresses();

    res.send(accounts);
  } catch (error) {
    res.status(error.statusCode || 500).send({ message: error.message });
  }
});

module.exports = router;
