const { Router } = require('express');
const config = require('config');
const mongoose = require('mongoose');

const router = new Router();
const mongodbStates = {
  0: 'disconnected',
  1: 'connected',
  2: 'connecting',
  3: 'disconnecting',
};

function getMongoDBStatus() {
  return mongodbStates[mongoose.connection.readyState] || 'unknown';
}

router.get('/check', (_, res) => {
  const mongoStatus = getMongoDBStatus();
  const statusCode = mongoStatus === 'connected' ? 200 : 500;

  res.status(statusCode).send({
    server: 'ok',
    mongodb: mongoStatus,
    environment: config.environment,
    uptime: process.uptime(),
    nodejs: process.version,
    process: {
      platform: process.platform,
      memory: process.memoryUsage(),
      pid: process.pid,
      cpuUsage: process.cpuUsage(),
      cwd: process.cwd(),
      arch: process.arch,
    },
  });
});

module.exports = router;
