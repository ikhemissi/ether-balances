const getAccountTransactions = require('./getAccountTransactions');
const getAccountBalance = require('./getAccountBalance');

function getTransactions({ address, fromBlock = 0 }) {
  return getAccountTransactions({ address, fromBlock, internal: false });
}

module.exports = {
  getTransactions,
  getAccountBalance,
};
