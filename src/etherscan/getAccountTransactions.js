const request = require('request-promise-native');
const { etherscan } = require('config');

async function getAccountTransactions({ address, fromBlock = 0, internal = false }) {
  const action = internal ? 'txlistinternal' : 'txlist';
  const uri = `${etherscan.url}/api?module=account&action=${action}&address=${address}&startblock=${fromBlock}&endblock=99999999&sort=asc&apikey=${etherscan.apiKey}`;
  const { status, result } = await request({
    uri,
    method: 'POST',
    json: true,
  });

  let transactions = result || [];
  if (status === '0') {
    transactions = [];
  }

  return transactions;
}

module.exports = getAccountTransactions;
