const request = require('request-promise-native');
const { etherscan } = require('config');

async function getAccountBalance({ address, block = 'latest' }) {
  const uri = `${etherscan.url}/api?module=account&action=balance&address=${address}&tag=${block}&apikey=${etherscan.apiKey}`;
  const { status, result } = await request({
    uri,
    method: 'POST',
    json: true,
  });

  if (status === '1') {
    return result;
  }

  throw new Error(`The balance could not be retrieved (status=${status})`);
}

module.exports = getAccountBalance;
