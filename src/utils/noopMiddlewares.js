const noopAccessMiddleware = (req, res, next) => {
  next();
};

const noopErrorMiddleware = (err, req, res, next) => {
  next();
};

module.exports = {
  noopAccessMiddleware,
  noopErrorMiddleware,
};
