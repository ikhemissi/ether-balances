function transactionWithDate({ transaction }) {
  if (transaction.timeStamp) {
    return Object.assign({
      date: new Date(Number(transaction.timeStamp) * 1000),
    }, transaction);
  }

  return transaction;
}

module.exports = transactionWithDate;
