global.fetch = require('node-fetch'); // very very ugly, but it seems cryptocompare needs 'fetch' to be available in the global scope. I will try to change this later
const BigNumber = require('bignumber.js');
const cryptocompare = require('cryptocompare');
const { formatEther } = require('ethers-utils');
const logger = require('./logger');

async function getBalanceValue({ wei, toOtherCurrencies = true }) {
  const eth = formatEther(wei);
  const balances = {
    wei,
    eth,
  };

  if (toOtherCurrencies) {
    try {
      const balanceInEther = new BigNumber(eth);
      const prices = await cryptocompare.price('ETH', ['BTC', 'USD', 'EUR', 'GBP']);

      balances.btc = balanceInEther.multipliedBy(prices.BTC).toString();
      balances.usd = balanceInEther.multipliedBy(prices.USD).toFormat(2);
      balances.eur = balanceInEther.multipliedBy(prices.EUR).toFormat(2);
      balances.gbp = balanceInEther.multipliedBy(prices.GBP).toFormat(2);
    } catch (error) {
      logger.error(error);
    }
  }

  return balances;
}

module.exports = getBalanceValue;
