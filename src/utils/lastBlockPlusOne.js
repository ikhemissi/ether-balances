const { bigNumberify } = require('ethers-utils');

function lastBlockPlusOne(lastBlock) {
  return bigNumberify(lastBlock).add(1).toString();
}

module.exports = lastBlockPlusOne;
