# ether-balances

> An interview  take home assignment 

## Introduction

An Etherscan proxy to access account balance and transactions.
The service provides a REST API to index data (fetched from Etherscan and persisted in DB) and then fetch it using various filters.
The data is stored in MongoDB in 2 collections: `accounts` for user account overview, and `transactions` for the list of transactions that were indexed.

Please check [the thought process](./docs/thought-process.md) guide for more details.

## Setup

To run the service, you can either use docker-compose or install all the tools and run them locally.

## With Docker Compose

After installing [docker-compose](https://docs.docker.com/compose/install/), please run the following command in the directory of the project:
```sh
docker-compose up
```

## Locally

To run the application locally, you will need to have the following dependencies:
- A running [MongoDB](https://docs.mongodb.com/manual/installation/)
- [NVM](https://github.com/creationix/nvm#installation) or [NodeJS](https://nodejs.org/en/download/) 10.5.0 installed 

Afterwards, you need to execute the following commands:
```sh
# Installing dependencies
nvm use # Use the right NodeJS version, check https://github.com/creationix/nvm for more details
npm install

# Starting the service, by default the API will listen on the port 9000
npm start
```

Now, you can access the service on the port 9000.

## Endpoints

### System status

#### GET /health/check

This endpoint returns the health check of the API and the status of its depencies.
By default, it will return a `200` status code, unless one of the dependencies is down, in which case it will return a `500` error message.

*Example*:
- Request: `curl http://localhost:9000/health/check | jq`
- Response:
```json
{
    "server": "ok",
    "mongodb": "connected",
    "environment": "development",
    "uptime": 33.494,
    "nodejs": "v10.5.0",
    "process": {
        "platform": "darwin",
        "memory": {
            "rss": 51097600,
            "heapTotal": 25473024,
            "heapUsed": 20165120,
            "external": 18281750
        },
        "pid": 94822,
        "cpuUsage": {
            "user": 740928,
            "system": 155199
        },
        "cwd": "/Users/iheb.khemissi/Apps/dapps/ether-balances",
        "arch": "x64"
    }
}
```

### Account details
The collection `/accounts` provides endpoints to update account details (e.g. fetch the lastest state from Etherscan) and to retrieve account details. 

#### GET /accounts
List all the accounts that have been indexed (data fetched from Etherscan).

*Example*:
- Request: `curl http://localhost:9000/accounts | jq`
- Response:
```json
[
    {
        "balance": "30406240631597340",
        "address": "0x2a0edec9454609d97e88d7259476ba4c25c761b1",
        "updatedAt": "2018-11-17T16:35:59.413Z"
    }
]
```

#### GET /accounts/:address
This endpoint allows retrieving the details of the account having the Ethereum address `:address`.

If the account was previously indexed (data fetched from Etherscan) then the account details will be returned as a JSON object.
Otherwise, a `404` response will be returned, asking the user to index this account.

*Example*:
- Request: `curl http://localhost:9000/accounts/0x2a0edec9454609d97e88d7259476ba4c25c761b1 | jq`
- Response:
```json
{
    "createdAt": "2018-11-17T14:10:10.969Z",
    "updatedAt": "2018-11-17T14:10:16.974Z",
    "balance": {
        "wei": "30406240631597340",
        "eth": "0.03040624063159734"
    },
    "lastBlock": "6716488",
    "transactions": [
        {
            "date": "2018-11-16T17:42:23.000Z",
            "timeStamp": "1542390143",
            "hash": "0x88247dd887bd539706323b74f67205f7a77443f30949e33c02d90e30ef73b260",
            "from": "0x2a0edec9454609d97e88d7259476ba4c25c761b1",
            "to": "0x1ce7ae555139c5ef5a57cc8d814a867ee6ee33d8",
            "value": "0"
        }
    ],
    "timestamp": 1542470991576
}
```

To get more information in the account description, please use these query parameters:

| Parameter    | Possible values    | Default    | Description                                                               |
|--------------|--------------------|------------|---------------------------------------------------------------------------|
| transactions | `overview`, `full` | `overview` | Use `full` to get full transaction details instead of the basic details   |
| exchange     | `true`, `false`    | `false`    | Use `true` to get the conversion of the balance in BTC, USD, EUR, and GBP |

NOTE: the transactions returned by this endpoint are limited to the 100 newest transactions. To get more transactions and have access to more filtering options, please check the `/accounts/:addresss/transactions`

#### GET /accounts/:address/transactions
Return all the account transactions.

If the account was previously indexed (data fetched from Etherscan) then the transactions will be returned as a JSON object.
Otherwise, a `404` response will be returned, asking the user to index this account.

*Example*:
- Request: `curl 'http://localhost:9000/accounts/0x2a0edec9454609d97e88d7259476ba4c25c761b1/transactions?fromDate=2018-11-16T15:33:15.000Z&toDate=2018-11-16T17:42:15.000Z&fromBlock=6716478' | jq`
- Response:
```json
[
    {
        "date": "2018-11-16T17:42:15.000Z",
        "blockNumber": 6716487,
        "hash": "0xa79c6ee7c0041c2fee0000099863842bc9b43cdcfa41b7aea007cf6ab1659d18",
        "from": "0x2a0edec9454609d97e88d7259476ba4c25c761b1",
        "to": "0x1ce7ae555139c5ef5a57cc8d814a867ee6ee33d8",
        "value": 30000000000000000
    },
    {
        "date": "2018-11-16T17:40:18.000Z",
        "blockNumber": 6716478,
        "hash": "0x10a1e2f1bba36ba48a8b60c7a3668268121556779c13cb52b75ab46754961ed8",
        "from": "0xac5d3845d820badc92e340ce66aa06d5d1e9504a",
        "to": "0x2a0edec9454609d97e88d7259476ba4c25c761b1",
        "value": 60000000000000000
    }
]
```

To filter the list, please check the table below for the list of query parameters that can be used to tune the result:

| Parameter | Possible values                                | Default    | Description                                                             |
|-----------|------------------------------------------------|------------|-------------------------------------------------------------------------|
| details   | `overview`, `full`                             | `overview` | Use `full` to get full transaction details instead of the basic details |
| fromDate  | Date string in the RFC2822 or ISO 8601 formats | None       | Only show transactions mined at or after this date                      |
| toDate    | Date string in the RFC2822or ISO 8601 formats  | None       | Only show transactions mined at or before this date                     |
| fromBlock | Block number (Integer)                         | None       | Only show transactions mined at or after this block                     |
| toBlock   | Block number (Integer)                         | None       | Only show transactions mined at or before this block                    |
| sort      | `newest`, `oldest`                             | `newest`   | How the transactions will be sorted                                     |


#### POST /accounts?address=:address
This endpoint allows indexing an account by fetching its latest transactions and balance from [Etherscan](https://etherscan.io) and storing it in MongoDB.

By calling this endpoint (no request body is needed), the API will return the newly fetched data.

*Example*:
- Request: `curl -X POST http://localhost:9000/accounts?address=0x2a0edec9454609d97e88d7259476ba4c25c761b1 | jq`
- Response:
```json
{
    "message": "The account was updated with 2 transactions",
    "address": "0x2a0edec9454609d97e88d7259476ba4c25c761b1",
    "balance": "30406240631597340",
    "lastBlock": "6716488",
    "newTransactions": [
        {
            "blockNumber": "6716488",
            "timeStamp": "1542390143",
            "hash": "0x6bd7a5eb43d708dcaee7f5998e560769c9d188914afab8fa1d359ead37fa2548",
            "nonce": "9",
            "blockHash": "0xddafaaf2308fa7c42a57845e5a0769a0427403a7c4968f59d706f4ab194f4429",
            "transactionIndex": "59",
            "from": "0x2a0edec9454609d97e88d7259476ba4c25c761b1",
            "to": "0x9195e00402abe385f2d00a32af40b271f2e87925",
            "value": "0",
            "gas": "250000",
            "gasPrice": "11000000000",
            "isError": "0",
            "txreceipt_status": "1",
            "input": "0x095ea7b30000000000000000000000001ce7ae555139c5ef5a57cc8d814a867ee6ee33d80000000000000000000000000000000000000000000055593713c61bf4300000",
            "contractAddress": "",
            "cumulativeGasUsed": "4984704",
            "gasUsed": "45675",
            "confirmations": "5116"
        },
        {
            "blockNumber": "6716488",
            "timeStamp": "1542390143",
            "hash": "0x88247dd887bd539706323b74f67205f7a77443f30949e33c02d90e30ef73b260",
            "nonce": "10",
            "blockHash": "0xddafaaf2308fa7c42a57845e5a0769a0427403a7c4968f59d706f4ab194f4429",
            "transactionIndex": "75",
            "from": "0x2a0edec9454609d97e88d7259476ba4c25c761b1",
            "to": "0x1ce7ae555139c5ef5a57cc8d814a867ee6ee33d8",
            "value": "0",
            "gas": "250000",
            "gasPrice": "11000000000",
            "isError": "0",
            "txreceipt_status": "1",
            "input": "0x338b5dea0000000000000000000000009195e00402abe385f2d00a32af40b271f2e879250000000000000000000000000000000000000000000055593713c61bf4300000",
            "contractAddress": "",
            "cumulativeGasUsed": "6696322",
            "gasUsed": "38634",
            "confirmations": "5116"
        }
    ]
}
```
