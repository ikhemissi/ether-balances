// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  clearMocks: true,
  coverageDirectory: 'coverage',
  testMatch: [
    '**/__tests__/**/*.spec.js?(x)',
  ],
  globalSetup: './__tests__/scripts/setup.js',
  globalTeardown: './__tests__/scripts/teardown.js',
  testEnvironment: './__tests__/scripts/mongo-environment.js',
};
