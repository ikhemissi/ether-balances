module.exports = {
  environment: 'default',
  server: {
    port: process.env.PORT || 9000,
  },
  mongodb: {
    connectionString: process.env.MONGO_URL || 'mongodb://localhost/etherbalances',
  },
  etherscan: {
    url: process.env.ETHERSCAN_URL || 'https://api.etherscan.io',
    apiKey: process.env.ETHERSCAN_API_KEY || '9GS6HNA5VV83GQ47QXUD85T9D7V51AVQ9I',
  },
  logger: {
    simple: false,
    enabled: true,
  },
};
