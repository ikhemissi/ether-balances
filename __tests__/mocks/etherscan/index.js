const config = require('config');
const nock = require('nock');
const account1Transactions = require('./0x2a0edec9454609d97e88d7259476ba4c25c761b1/transactions.json');
const account1Balance = require('./0x2a0edec9454609d97e88d7259476ba4c25c761b1/balance.json');

const account1Address = '0x2a0edec9454609d97e88d7259476ba4c25c761b1';

function activate() {
  nock(config.etherscan.url)
    .persist()
    .post('/api')
    .query(({ action, address }) => action === 'txlist' && address === account1Address)
    .reply(200, account1Transactions)
    .post('/api')
    .query(({ action, address }) => action === 'balance' && address === account1Address)
    .reply(200, account1Balance);

  if (!nock.isActive()) {
    nock.activate();
  }
}

function restore() {
  if (nock.isActive()) {
    nock.cleanAll();
    nock.restore();
  }
}

module.exports = {
  activate,
  restore,
};
