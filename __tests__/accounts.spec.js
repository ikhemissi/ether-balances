const mongoose = require('mongoose');
const request = require('supertest');
const database = require('../src/database');
const api = require('../src/api');
const etherscanMock = require('./mocks/etherscan');

describe('accounts', () => {
  beforeAll(async () => {
    etherscanMock.activate();
    // eslint-disable-next-line no-underscore-dangle
    await database.connect(global.__MONGO_URI__);
  });

  afterAll(async () => {
    etherscanMock.restore();
    await database.disconnect();
  });

  beforeEach((done) => {
    mongoose.connection.db.dropDatabase(done);
  });

  it('should return a 404 message if the account was not indexed', async () => {
    const server = request(api);

    return server
      .get('/accounts/0x2a0edec9454609d97e88d7259476ba4c25c761b1')
      .set('Accept', 'application/json')
      .expect(404, {
        message: 'This address has yet to be indexed',
      })
      .then();
  });

  it('should return all account new transactions since indexation', async () => {
    const server = request(api);

    return server
      .post('/accounts?address=0x2a0edec9454609d97e88d7259476ba4c25c761b1')
      .set('Accept', 'application/json')
      .expect(200)
      .expect((response) => {
        expect(response.body.address).toBe('0x2a0edec9454609d97e88d7259476ba4c25c761b1');
        expect(response.body.balance).toBe('30406240631597340');
        expect(response.body.lastBlock).toBe('6716488');
        expect(response.body.newTransactions.length).toBe(12);
      })
      .then();
  });

  it('should return the account details if it was previously indexed', async () => {
    const server = request(api);

    await server
      .post('/accounts?address=0x2a0edec9454609d97e88d7259476ba4c25c761b1')
      .set('Accept', 'application/json')
      .expect(200)
      .then();

    return server
      .get('/accounts/0x2a0edec9454609d97e88d7259476ba4c25c761b1')
      .set('Accept', 'application/json')
      .expect(200)
      .expect((response) => {
        expect(response.body.address).toBe('0x2a0edec9454609d97e88d7259476ba4c25c761b1');
        expect(response.body.balance).toEqual({ eth: '0.03040624063159734', wei: '30406240631597340' });
        expect(response.body.lastBlock).toBe('6716488');
        expect(response.body.transactions.length).toBe(12);
      })
      .then();
  });
});
